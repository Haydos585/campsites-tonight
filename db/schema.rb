# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160730090218) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bookings", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "rec_site_id"
    t.datetime "checkin_date"
    t.datetime "checkout_date"
    t.integer  "price"
    t.boolean  "paid"
    t.integer  "guests_adult"
    t.integer  "guests_child"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "uuid"
    t.index ["rec_site_id"], name: "index_bookings_on_rec_site_id", using: :btree
    t.index ["user_id"], name: "index_bookings_on_user_id", using: :btree
  end

  create_table "rec_sites", force: :cascade do |t|
    t.string   "object_id"
    t.string   "name"
    t.string   "label"
    t.string   "fac_type"
    t.date     "vers_date"
    t.string   "serial_no"
    t.string   "act_code"
    t.string   "reduce_na"
    t.decimal  "latitude"
    t.decimal  "longitude"
    t.decimal  "x_coord"
    t.decimal  "y_coord"
    t.string   "site_class"
    t.float    "fee"
    t.text     "tbva_c"
    t.string   "tb_visitor"
    t.string   "is_part_of"
    t.boolean  "dis_access"
    t.boolean  "bbq_elec"
    t.boolean  "bbq_gas"
    t.boolean  "bbq_pit"
    t.boolean  "bbq_wood"
    t.boolean  "camping"
    t.text     "camping_c"
    t.boolean  "caravan"
    t.text     "caravan_c"
    t.boolean  "fishing"
    t.text     "fishing_c"
    t.boolean  "fossicking"
    t.text     "fossicking_c"
    t.boolean  "hang_glide"
    t.text     "hang_glide_c"
    t.boolean  "heritage"
    t.text     "heritage_c"
    t.boolean  "paddling"
    t.text     "paddling_c"
    t.boolean  "picnicing"
    t.text     "picnicing_c"
    t.boolean  "reduce"
    t.text     "reduce_c"
    t.boolean  "rock_climb"
    t.text     "rock_climb_c"
    t.boolean  "walkingdog"
    t.text     "walkingdog_c"
    t.boolean  "wilderness"
    t.boolean  "wildlife"
    t.text     "wildlife_c"
    t.text     "comments"
    t.string   "photo_id_1"
    t.string   "photo_id_2"
    t.string   "photo_id_3"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "provider"
    t.string   "uid"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

end
