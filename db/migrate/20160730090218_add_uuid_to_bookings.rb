class AddUuidToBookings < ActiveRecord::Migration[5.0]
  def change
    add_column :bookings, :uuid, :string
  end
end
