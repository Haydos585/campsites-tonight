class CreateBookings < ActiveRecord::Migration[5.0]
  def change
    create_table :bookings do |t|
      t.belongs_to :user, index: true
      t.belongs_to :rec_site, index: true
      t.datetime :checkin_date
      t.datetime :checkout_date
      t.integer :price
      t.boolean :paid
      t.integer :guests_adult
      t.integer :guests_child

      t.timestamps
    end
  end
end
