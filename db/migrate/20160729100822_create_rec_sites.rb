class CreateRecSites < ActiveRecord::Migration[5.0]
  def change
    create_table :rec_sites do |t|
      t.string :object_id
      t.string :name
      t.string :label
      t.string :fac_type
      t.date :vers_date
      t.string :serial_no
      t.string :act_code
      t.string :reduce_na
      t.decimal :latitude
      t.decimal :longitude
      t.decimal :x_coord
      t.decimal :y_coord
      t.string :site_class
      t.float :fee
      t.text :tbva_c
      t.string :tb_visitor
      t.string :is_part_of
      t.boolean :dis_access
      t.boolean :bbq_elec
      t.boolean :bbq_gas
      t.boolean :bbq_pit
      t.boolean :bbq_wood
      t.boolean :camping
      t.text :camping_c
      t.boolean :caravan
      t.text :caravan_c
      t.boolean :fishing
      t.text :fishing_c
      t.boolean :fossicking
      t.text :fossicking_c
      t.boolean :hang_glide
      t.text :hang_glide_c
      t.boolean :heritage
      t.text :heritage_c
      t.boolean :paddling
      t.text :paddling_c
      t.boolean :picnicing
      t.text :picnicing_c
      t.boolean :reduce
      t.text :reduce_c
      t.boolean :rock_climb
      t.text :rock_climb_c
      t.boolean :walkingdog
      t.text :walkingdog_c
      t.boolean :wilderness
      t.boolean :wilderness
      t.boolean :wildlife
      t.text :wildlife_c
      t.text :comments
      t.string :photo_id_1
      t.string :photo_id_2
      t.string :photo_id_3

      t.timestamps
    end
  end
end
