$(document).ready(function() {
  $(".tab-nav span").on("click", function() {
    if ($(this).hasClass("active")) {
      // do nothing
    } else {
      var goingTo = $(this).attr('class');
      $(".tab-nav span").removeClass("active");
      $(this).toggleClass("active");
      $(".tab").addClass("hidden");
      $(".tab-"+goingTo).removeClass("hidden");
    }

  });
});
