
  $(".payment-method .active-payment").on("click", function() {
    $(".payment-method-options").toggleClass("visible");
  });

  $(".payment-method-options span").on("click", function() {
    $(".payment-method-options").removeClass("visible");
    $(".payment-method-options span").removeClass("current");
    $(this).addClass("current");
    $(".payment-method .active-payment").html($(this).html());

    // Set up pay now button
    $(".pay-now button .payment-type-text").html($(this).html());
  });

  // Hit the Pay Now button
  /*$(".pay-now button").on("click", function() {
    $(".modal-background").removeClass("hidden");
  });

  $(".confirm-purchase-modal .close").on("click", function() {
    $(".modal-background").addClass("hidden");
  });

  $(".confirm-purchase-modal button").on("click", function() {
    $(this).addClass("hidden");
    $(".loading-gif").removeClass("hidden");

    window.setTimeout(function() {
      window.location = window.location.pathname + "/confirmed";
    }, 2000);
  });*/
