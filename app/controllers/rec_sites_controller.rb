class RecSitesController < ApplicationController
  def index
    @rec_sites = RecSite.all
  end

  def show
    @recsite = RecSite.find(params[:id])
  end
end
