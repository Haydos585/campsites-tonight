class BookingsController < ApplicationController
  before_action :authenticate_user!
  def new
    @recsite = RecSite.find(params[:id])
  end

  def confirmed
    @recsite = RecSite.find(params[:id])

    @booking = Booking.new
    @booking.user_id = current_user.id
    @booking.rec_site_id = @recsite.id
    @booking.price = 30
    @booking.guests_adult = 6
    @booking.guests_child = 0
    @booking.uuid = SecureRandom.uuid
    @booking.save
  end

  def index
    @bookings = current_user.bookings
  end

  def show
    @booking = Booking.find_by uuid: params[:uuid]
  end
end
