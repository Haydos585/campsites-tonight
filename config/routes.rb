Rails.application.routes.draw do
  devise_for :users, :controllers => { :omniauth_callbacks => "callbacks" }

  root 'rec_sites#index'
  get 'campsite/:id', to: 'rec_sites#show'

  get 'campsites', to: 'rec_sites#index'
  get 'campsite/:id/book-now', to: 'bookings#new'
  get 'campsite/:id/book-now/confirmed', to: 'bookings#confirmed'

  get 'my-bookings', to: 'bookings#index'
  get 'my-bookings/:uuid', to: 'bookings#show'
end
